<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'categories' => 'Kategorijos',
    'cart' => 'Krepšelis',
    'search' => 'Ieškoti',
    'product' => 'Produktas',
    'shopping.cart' => 'Krepšelis',
    'home' => 'Namai',
    'catalog' => 'Katalogas',
    'addToCart' => 'Pridėti į krepšelį',
    
];

