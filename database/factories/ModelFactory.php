<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\Product::class, function (Faker\Generator $faker){
    return [
        'name' => $faker->word,
        'description' => $faker->text(),
        'price' => rand (1, 1000) / 10,
        'category_id' => rand (1, 3),
        'image_url' => $faker->imageUrl($width = 640, $height = 480)
    ];
});
