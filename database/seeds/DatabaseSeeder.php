<?php

use App\Models\Product;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        DB::table('categories')->insert([
            'name' => 'TV',
            'description' => str_random(100),
        ]);
        DB::table('categories')->insert([
            'name' => 'Baldai',
            'description' => str_random(100),
        ]);
        DB::table('categories')->insert([
            'name' => 'PC',
            'description' => str_random(100),
        ]);

        factory(Product::class, 50)->create();
    }
}
