<?php

namespace App\Providers;

use App\Models\Category;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
              view()->composer('layouts.navigation' ,function($view) {
                  $view->with('categories', Category::categories());
              });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
