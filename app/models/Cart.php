<?php

namespace App\Models;

class Cart
{
    /**
     * @var null
     */
    public $items = null;
    public $totalQuantity = 0;
    public $totalPrice = 0;

    /**
     * Gets old cart session
     *
     * @param Cart $oldCart
     */
    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQuantity = $oldCart->totalQuantity;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    /**
     * Adds item to cart array
     * @param Product $item
     * @param int $id
     */
    public function add($item, $id)
    {
        $storedItem = ['quantity' => 0, 'price' => $item->price, 'item' => $item];
        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
            }
        }
        
        $storedItem['quantity']++;
        $storedItem['price'] = $item->price * $storedItem['quantity'];
        $this->items[$id] = $storedItem;
        $this->totalQuantity++;
        $this->totalPrice += $item->price;
    }

    /**
     * Reduces item by one
     * @param int $id
     */
    public function reduceByOne($id)
    {
        $this->items[$id]['quantity']--;
        $this->items[$id]['price'] -= $this->items[$id]['item']['price'];
        $this->totalQuantity--;
        $this->totalPrice -= $this->items[$id]['item']['price'];
        if ($this->items[$id]['quantity'] <= 0) {
            unset($this->items[$id]);
        }
    }

    /**
     * Increases item by one
     * 
     * @param int $id
     */
    public function increaseByOne($id)
    {
        $this->items[$id]['quantity']++;
        $this->items[$id]['price'] += $this->items[$id]['item']['price'];
        $this->totalQuantity++;
        $this->totalPrice += $this->items[$id]['item']['price'];
    }

    /**
     * Removes item
     * 
     * @param int $id
     */
    public function removeItem($id)
    {
        $this->totalQuantity -= $this->items[$id]['quantity'];
        $this->totalPrice -= $this->items[$id]['item']['price'];
        unset($this->items[$id]);
    }

    /**
     * Clears cart array
     */
    public function clearCart()
    {
        $this->totalQuantity = 0;
        $this->totalPrice = 0;
        unset($this->items);
    }
}