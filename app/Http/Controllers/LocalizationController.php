<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Session;

class LocalizationController extends Controller
{

    /**
     * Switches between languages
     * 
     * @param string $lang
     *
     * @return mixed
     */
    public function switchLanguage($lang)
    {
        if (array_key_exists($lang, Config::get('languages'))) {
            Session::set('applocale', $lang);
        }

        return Redirect::back();
    }
}
