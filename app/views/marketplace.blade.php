<!-- CSS And JavaScript -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/product.css')}}">
@extends('layouts.navigation')

@section('content')
<div class="container-fluid">
    @if (count($products) > 0)
        <div class="row">
            @foreach ($products as $product)
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <a href="{{ route('product.single',['id' => $product->id]) }}">
                            @if ($product->image_url === null)
                                <img src="https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg"
                                     alt="..." class="img-responsive">
                            @else
                                <img class="img-responsive" src="{{$product->image_url}}" alt="...">
                            @endif
                        </a>

                        <div class="caption">
                            <a href="{{ route('product.single',['id' => $product->id]) }}">
                                <h3>{{ $product->name }}
                                </h3>
                            </a>
                            <small>Price: {{ $product->price }} €</small>
                            <p class="description">{{ $product->description }}</p>
                            <a href="{{ route('cart.add',['id' => $product->id]) }}"
                               class="btn btn-sucess" role="button">@lang('main.addToCart')</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
   @if (isset($products))
        {{ $products->links() }}
   @endif
</div>
@endsection