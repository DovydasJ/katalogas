<?php
Route::get('/', function(){
    return redirect()->route('product.all');
});

Route::get('/lang/{lang}', [
    'uses' => 'LocalizationController@switchLanguage',
    'as' => 'lang.switch'
]);

Route::group(['prefix' => 'product'], function () {
    Route::get('search', [
        'uses' => 'ProductController@search',
        'as' => 'product.search'
    ]);
    Route::get('all', [
        'uses' => 'ProductController@getProducts',
        'as' => 'product.all'
    ]);
    Route::get('category/{category}', [
       'uses' => 'ProductController@getCategoryProducts',
       'as' => 'product.category'
    ]);
    Route::get('{id}', [
        'uses' => 'ProductController@getProduct',
        'as' => 'product.single'
    ]);
    Route::get('add-to-cart/{id}', [
        'uses' => 'CartController@addToCart',
        'as' => 'cart.add'
    ]);
});

Route::group(['prefix' => 'shopping-cart'], function () {
    Route::get('/', [
        'uses' => 'CartController@showCart',
        'as' => 'cart.view'
    ]);
    Route::get('?token={token?}', [
        'uses' => 'CartController@getCart',
        'as' => 'cart.sharedView'
    ]);
    Route::get('/save', [
        'uses' => 'CartController@save',
        'as' => 'cart.save'
    ]);
    Route::get('/reduce/{id}', [
        'uses' => 'CartController@reduceItemByOne',
        'as' => 'cart.reduce'
    ]);
    Route::get('/increase/{id}', [
        'uses' => 'CartController@increaseItemByOne',
        'as' => 'cart.increase'
    ]);
    Route::get('/remove/{id}', [
        'uses' => 'CartController@removeItem',
        'as' => 'cart.removeItem'
    ]);

    Route::get('/clear', [
        'uses' => 'CartController@clear',
        'as' => 'cart.clear'
    ]);
});
